<?php

/**
 * @file
 * Contains functions that either override or extend webform module functions.
 */

/**
 * Overrides webform_download_filters_submit().
 */
function webform_download_filters_submit(&$form, &$form_state) {
  $options = array(
    'delimiter' => $form_state['values']['delimiter'],
    'components' => array_keys(array_filter($form_state['values']['components'])),
    'select_keys' => $form_state['values']['select_keys'],
    'select_format' => $form_state['values']['select_format'],
    'range_type' => $form_state['values']['range']['range_type'],
    'download' => $form_state['values']['download'],
    'webform_download_filters' => $form_state['values']['webform_download_filters'],
  );

  // Retrieve the list of required SIDs.
  if ($options['range_type'] != 'all') {
    $options['sids'] = webform_download_sids($form_state['values']['node']->nid, $form_state['values']['range']);
  }

  if ($options['webform_download_filters']['filter_component'] != 'weo_default') {
    $options['sids'] = webform_download_filters_sids($form_state['values']['node']->nid, $form_state['values']['webform_download_filters']);
  }

  $export_info = webform_results_export($form_state['values']['node'], $form_state['values']['format'], $options);

  // If webform result file should be downloaded, send the file to the browser,
  // otherwise save information about the created file in $form_state.
  if ($options['download']) {
    webform_results_download($form_state['values']['node'], $export_info);
  }
  else {
    $form_state['export_info'] = $export_info;
  }
}

/**
 * Get the sids based on conditions.
 *
 * Similar to webform_download_sids in webform module.
 */
function webform_download_filters_sids($nid, $webform_download_filters, $uid = NULL) {
  $cid = $webform_download_filters['filter_component'];
  $filter_condition = $webform_download_filters['filter_condition'];

  $sids = array();

  $filter_values = _webform_download_filters_convert_textarea_lines_to_array($webform_download_filters['filter_value']);

  $filters = array('nid' => $nid);
  $submissions = webform_get_submissions($filters);

  switch ($filter_condition) {
    case 'all':
      foreach ($submissions as $submission) {
        $sids[] = $submission->sid;
      }
      break;

    case 'is_in':
      // Is one of.
      foreach ($submissions as $submission) {
        if (!array_key_exists($cid, $submission->data)) {
          continue;
        }

        $inter = array_intersect($submission->data[$cid]['value'], $filter_values);
        if (!empty($inter)) {
          $sids[] = $submission->sid;
        }
      }
      break;

    case 'is_not_in':
      // Is not one of.
      foreach ($submissions as $submission) {
        $inter = array_intersect($submission->data[$cid]['value'], $filter_values);
        if (empty($inter)) {
          $sids[] = $submission->sid;
        }
      }
      break;

    case 'count_is':
      // Count is equal to.
      foreach ($submissions as $submission) {
        $count = count($submission->data[$cid]['value']);
        if ($count == $filter_values[0]) {
          $sids[] = $submission->sid;
        }
      }
      break;

    case 'count_is_not':
      // Count is not equal to.
      foreach ($submissions as $submission) {
        $count = count($submission->data[$cid]['value']);
        if ($count != $filter_values[0]) {
          $sids[] = $submission->sid;
        }
      }
      break;

    case 'count_is_lt':
      // Count is less than.
      foreach ($submissions as $submission) {
        $count = count($submission->data[$cid]['value']);
        if ($count < $filter_values[0]) {
          $sids[] = $submission->sid;
        }
      }
      break;

    case 'count_is_lte':
      // Count is less than or equal to.
      foreach ($submissions as $submission) {
        $count = count($submission->data[$cid]['value']);
        if ($count <= $filter_values[0]) {
          $sids[] = $submission->sid;
        }
      }
      break;

    case 'count_is_gt':
      // Count is greater than.
      foreach ($submissions as $submission) {
        $count = count($submission->data[$cid]['value']);
        if ($count > $filter_values[0]) {
          $sids[] = $submission->sid;
        }
      }
      break;

    case 'count_is_gte':
      // Count is greater than or equal to.
      foreach ($submissions as $submission) {
        $count = count($submission->data[$cid]['value']);
        if ($count >= $filter_values[0]) {
          $sids[] = $submission->sid;
        }
      }
      break;

  }

  if (empty($sids)) {
    $sids[] = -1;
  }

  return $sids;
}
