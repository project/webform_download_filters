------------------------------------------------------------------------------
                Webform Download Filters (webform_download_filters)
------------------------------------------------------------------------------

INTRODUCTION
------------
  This module provides data filtering options for the webform download page.

REQUIREMENTS
------------
  This module requires the following modules:
  Webform (https://drupal.org/project/webform)

INSTALLATION
------------
  Install as you would normally install a contributed Drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.

 
USAGE
-------------
  * Go to the Webform you want download data from.
  * Go Results -> Download.
  * Use the "Data Filter Options" section to select the component,
    the condition and the values.

HOOKS
-------------
  No additional hooks are made available by this module.
 
MAINTAINERS
-----------
  Current maintainers:
  Jaskaran Nagra - https://www.drupal.org/u/jaskaran.nagra
------------------------------------------------------------------------------
